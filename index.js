const tasks = [
  
];
const bodyHTMLElement = document.querySelector('body')
const tasksList = document.querySelector('.tasks-list');


const newTasks = (id, text) => {
    
    
    
    const divOne = document.createElement("div");
    divOne.className = "task-item";
    divOne.dataset.taskId = id;

    const divTwo = document.createElement('div');
    divTwo.className = 'task-item__main-container';

    const divThree = document.createElement('div');
    divThree.className = 'task-item__main-content';

    const form = document.createElement('form');
    form.className = 'checkbox-form';

    const input = document.createElement("input");
    input.className = 'checkbox-form__checkbox';
    input.type = 'checkbox';
    input.id = 'task-' + id;

    const label = document.createElement('label');
    label.htmlFor = 'task-' + id;

    const span = document.createElement('span');
    span.className = 'task-item__text';
    span.innerText = text;


    const button = document.createElement('button');
    button.className = 'task-item__delete-button default-button delete-button';
    button.dataset.deleteTaskId = '5';
    button.textContent = 'Delete';

    label.append(span)
    form.append(input);
    form.append(label);
    divThree.append(form);
    divThree.append(span)
    divTwo.append(divThree);
    divTwo.append(button);
    divOne.append(divTwo);
    const allTasks = document.querySelector('.tasks-list')
    allTasks.append(divOne)
}

for (let task of tasks)
    newTasks(task.id, task.text);



const  errorCreateSpan = (errorText) => {
    const errorCreateSpanTwo = document.createElement('span')
    errorCreateSpanTwo.className = 'error-message-block'
    errorCreateSpanTwo.textContent = errorText;
    return errorCreateSpanTwo
}

const createTaskForm = document.querySelector('.create-task-block')

createTaskForm.addEventListener('submit', (event) => {
    event.preventDefault()
    const {target} = event
    const taskNameInput = target.taskName
    const inputValue = taskNameInput.value
    let isValid = false

    if(inputValue) {
        isValid = true
        const tempArray = document.querySelectorAll('.task-item__text')
        tempArray.forEach(item => {
            if (inputValue === item.textContent) {
                isValid = false
            }
        })
    }

    if (!inputValue) {
        if (!document.querySelector('.error-message-block')) {
            createTaskForm.append(errorCreateSpan('Название задачи не должно быть пустым'))
        }
    }
    if (inputValue && !isValid) {
        if (!document.querySelector('.error-message-block')) {
            createTaskForm.append(errorCreateSpan('Задача с таким названием уже существует'))
        }
    }
    if (isValid) {
        const spanError = document.querySelector('.error-message-block')
        if (spanError) {
            spanError.remove()
        }
        const text = inputValue
        const id = Date.now()
        newTasks(id, text)
    }
});
const modalOverlay = document.createElement('div')
modalOverlay.className = 'modal-overlay modal-overlay_hidden'

const deleteModal = document.createElement('div')
deleteModal.className = 'delete-modal'
modalOverlay.append(deleteModal)

const deleteModalHeadline = document.createElement('h3')
deleteModalHeadline.className = 'delete-modal__question'
deleteModalHeadline.textContent = 'Delete Task?'
deleteModal.append(deleteModalHeadline)

const deleteModalButtons = document.createElement('div')
deleteModalButtons.className = 'delete-modal__buttons'
deleteModal.append(deleteModalButtons)

const deleteModalButtonCancel = document.createElement('button')
deleteModalButtonCancel.className = 'delete-modal__button delete-modal__cancel-button'
deleteModalButtonCancel.textContent = 'Cancel'
deleteModalButtons.append(deleteModalButtonCancel)


const deleteModalButtonConfirm = document.createElement('button')
deleteModalButtonConfirm.className = 'delete-modal__button delete-modal__confirm-button'
deleteModalButtonConfirm.textContent = 'Delete'
deleteModalButtons.append(deleteModalButtonConfirm)

bodyHTMLElement.append(modalOverlay)


const modalWindowOverlay = document.querySelector('.modal-overlay')
let idTaskDelete = ''

tasksList.addEventListener(`click`, (event) => {
    const deletePushButton = event.target.closest('.delete-button')

    if(deletePushButton){
        idTaskDelete = deletePushButton.parentElement.parentElement
        modalWindowOverlay.classList.remove('modal-overlay_hidden')
    }
})

modalWindowOverlay.addEventListener('click', (event) => {
    const buttonConfirmDelete = event.target.closest('.delete-modal__confirm-button')
    const buttonCancelDelete = event.target.closest('.delete-modal__cancel-button')

    if(buttonConfirmDelete){
        idTaskDelete.remove()
        modalWindowOverlay.classList.add('modal-overlay_hidden')
    } else if (buttonCancelDelete) {
        modalWindowOverlay.classList.add('modal-overlay_hidden')
    }
});

const bodyMain = document.body

const darkTheme = {
    bodyBgColor: `#24292E`,
    elemColor: `#ffffff`,
    buttonStyle: `1px solid #ffffff`
}

const whiteTheme = {
    bodyBgColor: `initial`,
    elemColor: `initial`,
    buttonStyle: `none`
}

function changeTheme() {
    bodyMain.style.backgroundColor = this.bodyBgColor

    const elemItem = document.querySelectorAll('.task-item')
    elemItem.forEach(value => value.style.color = this.elemColor)
    const allButton = document.querySelectorAll('button')
    allButton.forEach(value => value.style.border = this.buttonStyle)
}
let mainTheme = `whiteTheme`

bodyMain.addEventListener('keydown', (event) => {
    const { key } = event
    if (key === 'Tab' && mainTheme === `whiteTheme`) {
        changeTheme.call(darkTheme)
        mainTheme= `darkTheme`
    } else if (key === 'Tab' && mainTheme === `darkTheme`) {
        changeTheme.call(whiteTheme)
        mainTheme = `whiteTheme`
    }
});






